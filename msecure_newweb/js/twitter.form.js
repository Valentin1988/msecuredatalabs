$(document).ready(function () {
    setTimeout(function(){
        $('.block_news').css('height','350px');
        $('#twitter-widget-0').contents().find('.timeline-Footer').hide();
        $('#twitter-widget-0').contents().find('.timeline-Header-byline').hide();
    },1000);

    ! function(d, s, id) {

        var js, fjs = d.getElementsByTagName(s)[0],
            p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + "://platform.twitter.com/widgets.js";

            fjs.parentNode.insertBefore(js, fjs);
        }
    }(document, "script", "twitter-wjs");
    twttr.widgets.createTimeline(
        "600720083413962752",
        document.getElementById("container"),
        {
            height: 400,
            chrome: "nofooter",
            linkColor: "#820bbb",
            borderColor: "#a80000"
        }
    ); 
})
