$(document).ready(function() {
$.fn.simpleSpyc = function() {
    limit = 15;
    return this.each(function() {
        var c = $(this),
            b = true,
            a = [],
            d = 65;
        c.find("> li").each(function() {
            a.push("<li>" + $(this).html() + "</li>")
        });
        c.find("> li").filter(":gt(" + (limit - 1) + ")").remove();
        c.bind("stop", function() {
            b = false
        }).bind("start", function() {
            b = true
        });
        function e() {
            if (b) {
                var f = $(a[d]).css({
                    height: 0,
                    opacity: 0
                }).prependTo(c);
                c.find("> li:last").animate({
                    opacity: 0
                }, 0, function() {
                    $(this).animate({
                        height: 0
                    }, 0, function() {
                        $(this).remove()
                    })
                });
                f.animate({
                    opacity: 1
                }, 1).animate({
                    height: 25
                }, 500);
                d--;
                if (d < 0) {
                    d = 65
                }
            }
            setTimeout(e, 3000)
        }
        e()
    })
};
    $.getScript("//maps.google.com/maps/api/js?sensor=false&libraries=places&callback=initialize");
    $("ul.leadsCycleContainer_exchange3").simpleSpyc().bind("mouseenter", function() {
        $(this).trigger("stop")
    }).bind("mouseleave", function() {
        $(this).trigger("start")
    });
	/*$('[name^="filter-st-"]').on("change", function() {
        $("#filter-index-page").find("i.icon-dot-circle-o").removeClass("icon-dot-circle-o").addClass("icon-circle-o");
        $(this).parent().find("i").removeClass("icon-circle-o").addClass("icon-dot-circle-o");
        if ($(this).val() == 3) {
            $(".filter-tracking").show();
            $(".filter-shipping").hide()
        } else {
            $(".filter-tracking").hide();
            $(".filter-shipping").show()
        }
    });
   */
});


