<!DOCTYPE html>
<html>
<head>
    <title>Infections Map</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css" type="text/css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
	.logo-map-Box{
    		position: fixed;
    		z-index: 2222;
 		background: rgba(0,0,0,0.15);
    		right: 15px;
		float: right;
		padding: 5px 10px;
	    	border-radius: 14px;
	   	top: 40px;
	}
	#period{
		position: absolute;
		z-index: 999;
		right: 15px;
		top: 10px;
		padding: 4px;
		box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px;
		border-radius: 3px;
		border: 0 none;
		font-size: 12px;
		outline: none;
	}
        #map {
            height: 100%;
        }
        .colorPin{
            background: #fff;
        }
        .infowindow{
       	    background: #ffffb3;
       	    color: #ff471a;
        }
        .vir_head{
            margin-bottom: 0px;
            border-top: 1px solid #DDDDDD;
            padding-bottom: 0;
            padding-top: 2px;
        }
        #top{
            width: 50%;
	    background: #FFFFB3;
	    margin: 0 auto;
	    color: #ff471a;
        }
        #top p{
            text-align: center;
        }
        .top-virus{
            width: 100%;
        }
    </style>
</head>
<body>
<select name="days" id="period">
    <option value="1">Last 24 Hours</option>
    <option value="7">Last 7 Days</option>
    <option value="30">Last Month</option>
</select>
<div class="logo-map-Box">
	<div class="col-md-3 col-sm-3">
                <a href="../index.html">
                    <img src="../images/logo.png" alt="mmsecure">
                </a>
        </div>
</div>

<div id="map"></div>
<div id="top"></div>

<script>
    var map1;
    var markers = [];
    function initialize() {
        var latlng = new google.maps.LatLng(0, 0);

        var myOptions = {
            zoom: 2,
            minZoom: 2,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        map1 = new google.maps.Map(document.getElementById("map"), myOptions);
        var period = 1;
        getAllCountrisWithInfections(period);
    }


    $("#period").on('change', function () {

        markers = [];
        var latlng = new google.maps.LatLng(0, 0);
        var myOptions = {
            zoom: 2,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        map1 = new google.maps.Map(document.getElementById("map"), myOptions);
        var period = parseInt($(this).val());
        getAllCountrisWithInfections(period);

    });
    function getAllCountrisWithInfections(period){

        var periodDate = '';
        if (period == 1 || period == 7) {
            periodDate = getPeriodDate(period);
        }
        else if (period == 30) {
            periodDate = getLastMonthDate();
        }
        $.ajax({
            url: "server.php",
            dataType: 'JSON',
            type: 'POST',
            data: {'current': getCurrentDate(), 'period': periodDate},
            success: function (result) {
            	var top = result.top[0];
            	var topTable = "";
            	delete result.top;
            	topTable += "<p><strong>Top 10 Viruses</strong></p>";
            	topTable += "<table class='top-virus table-bordered'><thead><tr><th>Country</th><th>Virus name</th><th>Infections</th></tr></thead><tbody>";
            	$.each(top, function (index, value) {
            		topTable += "<tr><td>" + top[index].country + "</td><td>" + top[index].virus_name + "</td><td>" + 10000*top[index].infections + "</td></tr>";
            		
            	});
            	
            	topTable += "</tbody></table>";
            	$("#top").append(topTable);
            	$("#top").css("padding","20px");
                shownInfections(result);
            }
        });
    }

    function shownInfections(result){
        $.each(result, function (index, value) {

            var current = new google.maps.LatLng(value.lat, value.lng);

            markers.push(new google.maps.Marker({
                map: map1,
                position: current,
                center: current,
                icon: 'marker-threat.png',
                flat: true,
                title: value.country
            }));

            var virusesHTML ='';
            if(value.viruses.length > 0){
                virusesHTML += '<div><table class="table-bordered"><thead><tr><th>Country<br>' + value.country + '<p class="vir_head">Virusname</p></th><th>Infections<br>' + value.infections + '<p class="vir_head">Infections</p></th></tr></thead><tbody>';
                $.each(value.viruses, function(k,v){
                    virusesHTML += '<tr><td>'+v.virus_name+'</td><td>'+v.infections+'</td></tr>';
                });
                virusesHTML += '</tbody></table></div>';
            }
            markers[markers.length - 1]['infowin'] = new google.maps.InfoWindow({
                content: '<div class="infowindow">'+virusesHTML+'</div>'
            });

            google.maps.event.addListener(markers[markers.length - 1], 'click', function () {
                this['infowin'].open(map1, this);
            });
        })
    }
    function getPeriodDate(period) {

        var date = new Date();
        var dateInt = date.setDate(date.getDate() - period);
        var lastMontDate = new Date(dateInt);
        var year = lastMontDate.getFullYear();
        var month = lastMontDate.getMonth() + 1;
        if (month < 9)
            month = '0' + month;
        var day = lastMontDate.getDate();
        if (day < 10)
            day = '0' + day;
        return year + '-' + month + '-' + day;

    }

    function getLastMonthDate() {

        var date = new Date();
        var dateInt = date.setMonth(date.getMonth() - 1);
        var lastMontDate = new Date(dateInt);
        var year = lastMontDate.getFullYear();
        var month = lastMontDate.getMonth() + 1;
        if (month < 9)
            month = '0' + month;
        var day = lastMontDate.getDate();
        if (day < 10)
            day = '0' + day;
        return year + '-' + month + '-' + day;

    }


    function getCurrentDate() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 9)
            month = '0' + month;
        var day = date.getDate();
        if (day < 10)
            day = '0' + day;
        return year + '-' + month + '-' + day;

    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyArCAN4mlcbP7kYjkybsJcjAA3r8XGAj2k&callback=initialize">
</script>
</body>
</html>