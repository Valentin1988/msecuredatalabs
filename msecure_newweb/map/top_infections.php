<?php

$config = require_once __DIR__.'/config.php';

$mysqli_connect = mysqli_connect(
				$config['db']['hostname'],
				$config['db']['username'],
				$config['db']['password'],
				$config['db']['database']
				);
				
$query = "SELECT virus.name
	  FROM country_virus
	  INNER JOIN country ON country_virus.country_id = country.id
	  INNER JOIN virus ON country_virus.virus_id = virus.id
	  WHERE virus.name NOT LIKE  '%Android%'
	  GROUP BY country_virus.virus_id
	  ORDER BY SUM( country_virus.infections ) DESC 
	  LIMIT 6";
		
$res = mysqli_query($mysqli_connect, $query);

$topInfections = [];
while($result=mysqli_fetch_assoc($res)){
	$topInfections[] = $result;
}

echo json_encode($topInfections);

?>