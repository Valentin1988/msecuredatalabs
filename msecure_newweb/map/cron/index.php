<?php

$config = require_once '../config.php'; 

    $mysqli_connect = mysqli_connect(
									$config['db']['hostname'],
									$config['db']['username'],
									$config['db']['password'],
									$config['db']['database']
									);

mysqli_set_charset($mysqli_connect, "utf8");

$query = "SELECT * FROM country";

$result = mysqli_query($mysqli_connect, $query);
$rows = mysqli_num_rows($result);

if ($rows == 0) {
    $world_json = file_get_contents('countries.json');
    $world = json_decode($world_json, true);

    foreach ($world as $key => $value) {
        $query = "INSERT INTO country(name,lat,lng) VALUES ('" . $value['country'] . "','" . $value['lat'] . "','" . $value['lng'] . "')";
        $res = mysqli_query($mysqli_connect, $query);
    }
}

$files = scandir(__DIR__);

$dataZip = array();
$json_vdb = array();
foreach ($files as $key => $value) {
    if (strpos($value, 'data.zip-') !== false) {

        $dataZip[$key] = substr($value, 9);

        $renamedFileName = substr($value, 9) . "-data.zip";

        rename($value, $renamedFileName);

        $zip = new ZipArchive();
        $res = $zip->open($renamedFileName);
        if ($res === TRUE) {
            $zip->extractTo(__DIR__ . '/data-' . substr($value, 9));
            $zip->close();
        } else {
            echo 'Failed to open data.zip';
        }
        //unlink($renamedFileName);
    }
    if (strpos($value, 'vdb_info.json-') !== false) {

        $json_vdb[$key] = substr($value, 14);

        $renamedFileName = substr($value, 14) . "-vdb_info.json";

        rename($value, $renamedFileName);
    }
}
$files = scandir(__DIR__);

foreach ($files as $key => $value) {
    if (strpos($value, '-vdb_info.json') !== false) {
        $virusname_json = file_get_contents($value);
        $virusname = json_decode($virusname_json, true);
        $viruses = $virusname['new detections'];
        foreach ($viruses as $item => $virus) {
            $query = "SELECT * FROM virus WHERE name='" . $virus . "'";
            $result = mysqli_query($mysqli_connect, $query);

            $rows = mysqli_num_rows($result);
            if ($rows == 0) {
                $query = "INSERT INTO virus(name) VALUES ('" . $virus . "')";
                $res = mysqli_query($mysqli_connect, $query);
            }
        }
    }
}

foreach ($files as $datakey => $datavalue) {
    if (strpos($datavalue, 'data-') !== false) {
        $virnamefiles = scandir(__DIR__ . '/' . $datavalue . '/data/virname');
        foreach ($virnamefiles as $virusitem => $virnamefile) {
            if (strpos($virnamefile, '.json') !== false) {

                $virnamefile_json = file_get_contents(__DIR__ . '/' . $datavalue . '/data/virname/' . $virnamefile);
                $virnamefile_json_decode = json_decode($virnamefile_json, true);

                $query = "SELECT id FROM virus WHERE name='" . $virnamefile_json_decode['virnames'][0]['name'] . "'";
                $result = mysqli_query($mysqli_connect, $query);
                $rows = mysqli_num_rows($result);

                if($rows != 0){
                    $virus_id = mysqli_fetch_assoc($result)['id'];
                    foreach ($virnamefile_json_decode['countries'] as $countryKey => $country) {
                        if ($country['country'] != 'All') {
                            $country['country'] = mysqli_real_escape_string($mysqli_connect, $country['country']);
                            $query = "SELECT id FROM country WHERE name='" . $country['country'] . "'";

                            $res = mysqli_query($mysqli_connect, $query);
                            $country_id = mysqli_fetch_assoc($res)['id'];
                            if($country_id == 0){
                                echo $country['country'];die;
                            }

                            $infections = $country['infections'];
                            if($infections!=0){
                                $virus_date = $virnamefile_json_decode['date'][count($virnamefile_json_decode['date']) - 1]['date'];

                                $query = "INSERT INTO country_virus (country_id,virus_id,infections,virus_date)
                                                  VALUES ('" . $country_id . "','" . $virus_id . "','" . $infections . "','" . strtotime($virus_date) . "')";
                                $res = mysqli_query($mysqli_connect, $query);
                            }

                        }

                    }
                }
            }

        }

    }
}
?>