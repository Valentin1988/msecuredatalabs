<?php

$config = require_once __DIR__.'/config.php'; 

if (!empty($_POST) && isset($_POST['current']) && isset($_POST['period'])) {

    $current = strtotime($_POST['current']);
    $period = strtotime($_POST['period']);
	
	
    $mysqli_connect = mysqli_connect(
									$config['db']['hostname'],
									$config['db']['username'],
									$config['db']['password'],
									$config['db']['database']
									);

    $query = "SELECT
                country.name as country,
                country.lat as lat,
                country.lng as lng,
                virus.name as virus_name,
                SUM(country_virus.infections) as infections,
                country_virus.virus_date
                FROM country_virus
                INNER JOIN country ON country_virus.country_id = country.id
                INNER JOIN virus ON country_virus.virus_id = virus.id
                WHERE country_virus.virus_date BETWEEN '" . $period . "' AND '" . $current . "'
                GROUP BY country_virus.country_id, country_virus.virus_id
		ORDER BY SUM( country_virus.infections ) DESC";
                

    $result = mysqli_query($mysqli_connect, $query);
    $country_list_with_viruses = array();
    $topCountryVirusList = [];
    $limit = 0;
    if ($result->num_rows > 0) {
        while ($rows = mysqli_fetch_assoc($result)) {
            $country_list_with_viruses[] = $rows;
            if(strpos($rows['virus_name'], 'Android') === false && $limit!=10){
            	$topCountryVirusList[] = $rows;
            	$limit++;
            }
        }
    }
    else
    {
        $query = "SELECT
                country.name as country,
                country.lat as lat,
                country.lng as lng,
                virus.name as virus_name,
                SUM(country_virus.infections) as infections,
                country_virus.virus_date
                FROM country_virus
                INNER JOIN country ON country_virus.country_id = country.id
                INNER JOIN virus ON country_virus.virus_id = virus.id
                WHERE country_virus.virus_date = (SELECT MAX(country_virus.virus_date) FROM country_virus)
                GROUP BY country_virus.country_id, country_virus.virus_id
                ORDER BY SUM( country_virus.infections ) DESC";

    $result_new = mysqli_query($mysqli_connect, $query);

    if ($result_new->num_rows > 0) {
        while ($rows = mysqli_fetch_assoc($result_new)) {
            $country_list_with_viruses[] = $rows;
            if(strpos($rows['virus_name'], 'Android') === false && $limit!=10){
            	$topCountryVirusList[] = $rows;
            	$limit++;
            }
        }
    }
    
    }
    $country_list = array();
    foreach ($country_list_with_viruses as $key => $value) {

        $country_list[$key]['country'] = $value['country'];
        $country_list[$key]['lat'] = $value['lat'];
        $country_list[$key]['lng'] = $value['lng'];
        $country_list[$key]['infections'] = 0;
        $country_list[$key]['viruses'] = array();
        foreach ($country_list_with_viruses as $k => $v) {
            if ($v['country'] == $value['country']) {
                $country_list[$key]['infections'] += 10000*$value['infections'];
                ini_set('memory_limit', '-1');
                $country_list[$key]['viruses'][] = array(
                    'virus_name' => $v['virus_name'],
                    'infections' => 10000*$v['infections']
                );
            }
        }


    }
    
    /*echo '<pre>';
    print_r($country_list);
    echo '</pre>';
    die;*/
    
    $country_list['top'][] = $topCountryVirusList;

    echo json_encode($country_list);

}
?>