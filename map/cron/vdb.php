<?php

$con = require_once __DIR__ . "/config.php";

$mysqli_connect = mysqli_connect($con['db']['hostname'], $con['db']['username'], $con['db']['password'], $con['db']['database']);

$files = scandir(__DIR__ . '/vdb');

foreach($files as $value) {

    if (strpos($value, 'vdb_info.json-') !== false) {
        $virusname_json = file_get_contents(__DIR__ . '/vdb/' . $value);
        $virusname = json_decode($virusname_json, true);
        $build = $virusname['vdb build'];
        $date = substr($virusname['date'], 0, 10);
        $virusname = array_merge($virusname['new threats'], $virusname['new detections']);

        foreach ($virusname as $item => $virus) {
            $query = "INSERT INTO vdb_info(build, virus_name, date)
                      VALUES ('" . $build . "','" . $virus . "','" . $date . "')";
            $res = mysqli_query($mysqli_connect, $query);
        }
    }
}

?>