<?php

$con = require_once __DIR__ . "/config.php";

$mysqli_connect = mysqli_connect(
				$con['db']['hostname'],
				$con['db']['username'],
				$con['db']['password'],
				$con['db']['database']
				);
				
$query = "SELECT * FROM vdb_info GROUP BY build, date";

$res = mysqli_query($mysqli_connect, $query);

$vdb = [];

while($result=mysqli_fetch_assoc($res)){
    $vdb[] = $result;
}

echo json_encode($vdb);


?>