<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>VDB build details</title>
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<style>
		    .VDB-title{
		    	 color: #DE0007;
		    	 border-bottom: 1px solid;
   			 padding: 10px 0;
		    }
		</style>
	</head>
	<body>
	<div class="container">
		<h1 class="VDB-title">VDB <?= $_GET['build'] ?> build version details</h1>
		<?php


		$con = require_once __DIR__ . "/config.php";
		
		$mysqli_connect = mysqli_connect(
						$con['db']['hostname'],
						$con['db']['username'],
						$con['db']['password'],
						$con['db']['database']
						);
						
		if(isset($_GET['build']) && !empty($_GET['build']) && isset($_GET['date']) && !empty($_GET['date'])){
		
			$query = "SELECT * FROM vdb_info WHERE build = '" . $_GET['build'] . "' AND date = '" . $_GET['date'] . "'";
			
			$res = mysqli_query($mysqli_connect, $query);
			
			$vdb = [];
			
			echo '<ul>';
		
			while($result=mysqli_fetch_assoc($res)){
			    echo '<li>' . $result['virus_name'] . '</li>';
			}
			
			echo '</ul>';
		
		
		}
		
		?>
	</div>	
	</body>
</html>
