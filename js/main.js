$.fn.simpleSpy = function() {
    limit = 8;
    return this.each(function() {
        var c = $(this),
            b = true,
            a = [],
            d = 9;
        c.find("> li").each(function() {
            a.push("<li>" + $(this).html() + "</li>")
        });
        c.find("> li").filter(":gt(" + (limit - 1) + ")").remove();
        c.bind("stop", function() {
            b = false
        }).bind("start", function() {
            b = true
        });
        function e() {
            if (b) {
                var f = $(a[d]).css({
                    height: 0,
                    opacity: 0
                }).prependTo(c);
                c.find("> li:last").animate({
                    opacity: 0
                }, 0, function() {
                    $(this).animate({
                        height: 0
                    }, 0, function() {
                        $(this).remove()
                    })
                });
                f.animate({
                    opacity: 1
                }, 1).animate({
                    height: 34
                }, 500);
                d--;
                if (d < 0) {
                    d = 9
                }
            }
            setTimeout(e, 4000)
        }
        e()
    })
};
var SHIPMENT = 1;

function update_shipment() {
    if (SHIPMENT == 1) {
        $("#btn-shipment").html('<i class="sprite sprite-container"></i> <span class="ml38">FCL <i class="icon-angle-down pl5"></i></span>')
    } else {
        if (SHIPMENT == 2) {
            $("#btn-shipment").html('<i class="sprite sprite-box"></i> <span class="ml38">LCL <i class="icon-angle-down pl5"></i></span>')
        } else {
            if (SHIPMENT == 3) {
                $("#btn-shipment").html('<i class="sprite sprite-truck"></i> <span class="ml38">FTL <i class="icon-angle-down pl5"></i></span>')
            } else {
                if (SHIPMENT == 4) {
                    $("#btn-shipment").html('<i class="sprite sprite-airplane"></i> <span class="ml38">Air <i class="icon-angle-down pl5"></i></span>')
                }
            }
        }
    }
}
$(document).ready(function() {
    update_shipment();
    $.getScript("//maps.google.com/maps/api/js?sensor=false&libraries=places&callback=initialize");
    $("ul.leadsCycleContainer_exchange").simpleSpy().bind("mouseenter", function() {
        $(this).trigger("stop")
    }).bind("mouseleave", function() {
        $(this).trigger("start")
    });
    $('[name^="filter-st-"]').on("change", function() {
        $("#filter-index-page").find("i.icon-dot-circle-o").removeClass("icon-dot-circle-o").addClass("icon-circle-o");
        $(this).parent().find("i").removeClass("icon-circle-o").addClass("icon-dot-circle-o");
        if ($(this).val() == 3) {
            $(".filter-tracking").show();
            $(".filter-shipping").hide()
        } else {
            $(".filter-tracking").hide();
            $(".filter-shipping").show()
        }
    });
});
/************************************/
$.fn.simpleSpyn = function() {
    limit = 4;
    return this.each(function() {
        var c = $(this),
            b = true,
            a = [],
            d = 9;
        c.find("> li").each(function() {
            a.push("<li>" + $(this).html() + "</li>")
        });
        c.find("> li").filter(":gt(" + (limit - 1) + ")").remove();
        c.bind("stop", function() {
            b = false
        }).bind("start", function() {
            b = true
        });
        function e() {
            if (b) {
                var f = $(a[d]).css({
                    height: 0,
                    opacity: 0
                }).prependTo(c);
                c.find("> li:last").animate({
                    opacity: 0
                }, 0, function() {
                    $(this).animate({
                        height: 0
                    }, 0, function() {
                        $(this).remove()
                    })
                });
                f.animate({
                    opacity: 1
                }, 1).animate({
                    height: 80
                }, 500);
                d--;
                if (d < 0) {
                    d = 9
                }
            }
            setTimeout(e, 4000)
        }
        e()
    })
};
var SHIPMENTn = 1;

function update_shipmentn() {
    if (SHIPMENTn == 1) {
        $("#btn-shipment").html('<i class="sprite sprite-container"></i> <span class="ml38">FCL <i class="icon-angle-down pl5"></i></span>')
    } else {
        if (SHIPMENTn == 2) {
            $("#btn-shipment").html('<i class="sprite sprite-box"></i> <span class="ml38">LCL <i class="icon-angle-down pl5"></i></span>')
        } else {
            if (SHIPMENTn == 3) {
                $("#btn-shipment").html('<i class="sprite sprite-truck"></i> <span class="ml38">FTL <i class="icon-angle-down pl5"></i></span>')
            } else {
                if (SHIPMENTn == 4) {
                    $("#btn-shipment").html('<i class="sprite sprite-airplane"></i> <span class="ml38">Air <i class="icon-angle-down pl5"></i></span>')
                }
            }
        }
    }
}
$(document).ready(function() {
    update_shipmentn();
    $.getScript("//maps.google.com/maps/api/js?sensor=false&libraries=places&callback=initialize");
    $("ul.leadsCycleContainer_exchange1").simpleSpyn().bind("mouseenter", function() {
        $(this).trigger("stop")
    }).bind("mouseleave", function() {
        $(this).trigger("start")
    });
    $('[name^="filter-st-"]').on("change", function() {
        $("#filter-index-page").find("i.icon-dot-circle-o").removeClass("icon-dot-circle-o").addClass("icon-circle-o");
        $(this).parent().find("i").removeClass("icon-circle-o").addClass("icon-dot-circle-o");
        if ($(this).val() == 3) {
            $(".filter-tracking").show();
            $(".filter-shipping").hide()
        } else {
            $(".filter-tracking").hide();
            $(".filter-shipping").show()
        }
    });
});
/************************************/
$.fn.simpleSpyr = function() {
    limit = 6;
    return this.each(function() {
        var c = $(this),
            b = true,
            a = [],
            d = 9;
        c.find("> li").each(function() {
            a.push("<li>" + $(this).html() + "</li>")
        });
        c.find("> li").filter(":gt(" + (limit - 1) + ")").remove();
        c.bind("stop", function() {
            b = false
        }).bind("start", function() {
            b = true
        });
        function e() {
            if (b) {
                var f = $(a[d]).css({
                    height: 0,
                    opacity: 0
                }).prependTo(c);
                c.find("> li:last").animate({
                    opacity: 0
                }, 0, function() {
                    $(this).animate({
                        height: 0
                    }, 0, function() {
                        $(this).remove()
                    })
                });
                f.animate({
                    opacity: 1
                }, 1).animate({
                    height: 32
                }, 500);
                d--;
                if (d < 0) {
                    d = 9
                }
            }
            setTimeout(e, 4000)
        }
        e()
    })
};
var SHIPMENTr = 1;

function update_shipmentr() {
    if (SHIPMENTr == 1) {
        $("#btn-shipment").html('<i class="sprite sprite-container"></i> <span class="ml38">FCL <i class="icon-angle-down pl5"></i></span>')
    } else {
        if (SHIPMENTr == 2) {
            $("#btn-shipment").html('<i class="sprite sprite-box"></i> <span class="ml38">LCL <i class="icon-angle-down pl5"></i></span>')
        } else {
            if (SHIPMENTr == 3) {
                $("#btn-shipment").html('<i class="sprite sprite-truck"></i> <span class="ml38">FTL <i class="icon-angle-down pl5"></i></span>')
            } else {
                if (SHIPMENTr == 4) {
                    $("#btn-shipment").html('<i class="sprite sprite-airplane"></i> <span class="ml38">Air <i class="icon-angle-down pl5"></i></span>')
                }
            }
        }
    }
}
$(document).ready(function() {
    update_shipmentr();
    $.getScript("//maps.google.com/maps/api/js?sensor=false&libraries=places&callback=initialize");
    $("ul.leadsCycleContainer_exchange2").simpleSpyr().bind("mouseenter", function() {
        $(this).trigger("stop")
    }).bind("mouseleave", function() {
        $(this).trigger("start")
    });
    $('[name^="filter-st-"]').on("change", function() {
        $("#filter-index-page").find("i.icon-dot-circle-o").removeClass("icon-dot-circle-o").addClass("icon-circle-o");
        $(this).parent().find("i").removeClass("icon-circle-o").addClass("icon-dot-circle-o");
        if ($(this).val() == 3) {
            $(".filter-tracking").show();
            $(".filter-shipping").hide()
        } else {
            $(".filter-tracking").hide();
            $(".filter-shipping").show()
        }
    });
});

/************************************/
$.fn.simpleSpyc = function() {
    limit = 4;
    return this.each(function() {
        var c = $(this),
            b = true,
            a = [],
            d = 9;
        c.find("> li").each(function() {
            a.push("<li>" + $(this).html() + "</li>")
        });
        c.find("> li").filter(":gt(" + (limit - 1) + ")").remove();
        c.bind("stop", function() {
            b = false
        }).bind("start", function() {
            b = true
        });
        function e() {
            if (b) {
                var f = $(a[d]).css({
                    height: 0,
                    opacity: 0
                }).prependTo(c);
                c.find("> li:last").animate({
                    opacity: 0
                }, 0, function() {
                    $(this).animate({
                        height: 0
                    }, 0, function() {
                        $(this).remove()
                    })
                });
                f.animate({
                    opacity: 1
                }, 1).animate({
                    height: 103
                }, 500);
                d--;
                if (d < 0) {
                    d = 9
                }
            }
            setTimeout(e, 4000)
        }
        e()
    })
};
var SHIPMENTc = 1;

function update_shipmentc() {
    if (SHIPMENTc == 1) {
        $("#btn-shipment").html('<i class="sprite sprite-container"></i> <span class="ml38">FCL <i class="icon-angle-down pl5"></i></span>')
    } else {
        if (SHIPMENTc == 2) {
            $("#btn-shipment").html('<i class="sprite sprite-box"></i> <span class="ml38">LCL <i class="icon-angle-down pl5"></i></span>')
        } else {
            if (SHIPMENTc == 3) {
                $("#btn-shipment").html('<i class="sprite sprite-truck"></i> <span class="ml38">FTL <i class="icon-angle-down pl5"></i></span>')
            } else {
                if (SHIPMENTc == 4) {
                    $("#btn-shipment").html('<i class="sprite sprite-airplane"></i> <span class="ml38">Air <i class="icon-angle-down pl5"></i></span>')
                }
            }
        }
    }
}
$(document).ready(function() {

    update_shipmentc();
    $.getScript("//maps.google.com/maps/api/js?sensor=false&libraries=places&callback=initialize");
    $("ul.leadsCycleContainer_exchange3").simpleSpyc().bind("mouseenter", function() {
        $(this).trigger("stop")
    }).bind("mouseleave", function() {
        $(this).trigger("start")
    });
    $('[name^="filter-st-"]').on("change", function() {
        $("#filter-index-page").find("i.icon-dot-circle-o").removeClass("icon-dot-circle-o").addClass("icon-circle-o");
        $(this).parent().find("i").removeClass("icon-circle-o").addClass("icon-dot-circle-o");
        if ($(this).val() == 3) {
            $(".filter-tracking").show();
            $(".filter-shipping").hide()
        } else {
            $(".filter-tracking").hide();
            $(".filter-shipping").show()
        }
    });
});


